/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* INTEGRANTES: JORGE CARRILLO
 *              CRISTÓBAL BRICEÑO
 */

/******************** Servidor.- ********************/

/*
 * file Servidor.cpp
 * g++ Servidor.cpp -pthread -o Servidor
 * make Servidor.cpp
 *      Servidor.o
 *      Server
 */

//* Servidor.cpp - Programa principal (Servidor).

// Llamar e incluir librerías necesarias.
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <pthread.h>
#include <semaphore.h>
#include <string>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <unistd.h>

// Prototipos de funciones.
void* clientHandler(void*);
int main(int, char*[]);

// Variables constantes
const int PORT = 8080;
const int MAX_CONNECTIONS = 5;
#define BUFFER_SIZE 9999

// Inicializar hebras.
pthread_t clientThread;

using namespace std;


//* ----- CUERPO DEL CÓDIGO ----- *//

//* Función Receptor del cliente.
// Ejecuta en hilos las conexiones del cliente.
void* clientHandler(void* socketDesc) {
	int socketCliente = *(int*)socketDesc;
	char buffer[BUFFER_SIZE];

	// Recibir y guardar los mensajes del cliente
	while (true) {
		memset(buffer, 0, sizeof(buffer));
		int bytesRead = recv(socketCliente, buffer, sizeof(buffer) - 1, 0);
		
		if (bytesRead <= 0) {
			break;  // Terminar si hay error o la conexión se cierra
		}
		
		// Guardar el mensaje recibido
		cout << "- Mensaje recibido: " << buffer << endl;

		// Verificar si se recibió la señal de finalización
		if (strcmp(buffer, "FINISHED") == 0) {
			break; 			// Terminar el bucle si se recibió la señal de finalización
		}
	}
	
	close(socketCliente);
	delete (int*)socketDesc;
	pthread_exit(NULL);
}

//* Función Main.
// Función principal del Servidor y configuraciones previas de conexión usando pthreads y sockets
int main (int argc, char *argv[]) {
	int serverSocket, socketCliente;
	sockaddr_in addrServer, addrCliente;
	socklen_t LENaddrCliente = sizeof(addrCliente);
	
	// Crear el socket del servidor
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	
	if (serverSocket == -1) {
		cerr << "\t -------------------------------------------------- \n";
		cerr << "\t ***** ERROR: FALLO EN LA CREACIÓN DE SOCKET. ***** \n";
		cerr << "\t -------------------------------------------------- \n";
		exit(EXIT_FAILURE);	
	}
	
	// Configurar la dirección del servidor
	addrServer.sin_family = AF_INET;
	addrServer.sin_addr.s_addr = INADDR_ANY;
	addrServer.sin_port = htons(PORT);
	
	// Vincular el socket a la dirección del servidor
	if (bind(serverSocket, (struct sockaddr*)&addrServer, sizeof(addrServer)) == -1) {
		cerr << "\t ------------------------------------------------------------------ \n";
		cerr << "\t ***** ERROR: FALLO EN LA VINCULACIÓN DEL SOCKET AL SERVIDOR. ***** \n";
		cerr << "\t ------------------------------------------------------------------ \n";
		close(serverSocket);
		exit(EXIT_FAILURE);	
	}
	
	// Escuchar por conexiones entrantes
	if (listen(serverSocket, MAX_CONNECTIONS) == -1) {
		cerr << "\t ------------------------------------------------------- \n";
		cerr << "\t ***** ERROR AL ESCUCHAR POR CONEXIONES ENTRANTES. ***** \n";
		cerr << "\t ------------------------------------------------------- \n";
		close(serverSocket);
		exit(EXIT_FAILURE);	
	}

	cout << "\033c";
	cout << "\n\t ····· SERVIDOR EN ESPERA DE CONEXIONES. ····· \n" << endl;
	
	// Aceptar y manejar las conexiones entrantes
	while (true) {
		socketCliente = accept(serverSocket, (struct sockaddr*)&addrCliente, &LENaddrCliente);
		if (socketCliente == -1) {
			cerr << "\t ----------------------------------------------- \n";
			cerr << "\t ***** ERROR: CONEXIÓN ENTRANTE RECHAZADA. ***** \n";
			cerr << "\t ----------------------------------------------- \n";
			close(serverSocket);
			exit(EXIT_FAILURE);	
		}

		// Crear un hilo para manejar el cliente
		int* socketDesc = new int;
		*socketDesc = socketCliente;
		
		if (pthread_create(&clientThread, NULL, clientHandler, (void*)socketDesc) != 0) {
			cerr << "\t ------------------------------------------------- \n";
			cerr << "\t ***** ERROR: FALLO EN LA CREACIÓN DE HILOS. ***** \n";
			cerr << "\t ------------------------------------------------- \n";
			close(serverSocket);
			exit(EXIT_FAILURE);	
		}
		
		// Liberar los recursos del hilo cuando termine
		pthread_detach(clientThread);
	}
	
	close(serverSocket);
	return 0;
}
