/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* INTEGRANTES: JORGE CARRILLO
 *              CRISTÓBAL BRICEÑO
 */

/******************** Cliente.- ********************/

/*
 * file Cliente.cpp
 * g++ Cliente.cpp -pthread -o Cliente
 * make Cliente.cpp
 *      Cliente.o
 *      Cliente
 */

//* Cliente.cpp - Programa principal (Cliente).

// Llamar e incluir librerías necesarias.
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <pthread.h>
#include <semaphore.h>
#include <string>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <unistd.h>

// Prototipos de funciones.
void* receiveMessages(void*);
void alignmentMethods();
int main();

// Variables constantes
const char* SERVER_IP = "127.0.0.1";
const int SERVER_PORT = 8080;
#define BUFFER_SIZE 9999

// Inicializar hebras.
pthread_t receiveThread;

using namespace std;


//* ----- CUERPO DEL CÓDIGO ----- *//

//* Función Receptor de mensajes.
// Función que, utilizando una combinación de punteros, pthreads y asignación de memoria, recibe y muestra los mensajes 
// del servidor.
void* receiveMessages(void* socketDesc) {
	int serverSocket = *(int*)socketDesc;
	char buffer[BUFFER_SIZE];
	
	// Recibir y mostrar los mensajes del servidor
	while (true) {
		memset(buffer, 0, sizeof(buffer));
		int bytesRead = recv(serverSocket, buffer, sizeof(buffer) - 1, 0);
		
		if (bytesRead <= 0) {
			break;			// Terminar si hay error o la conexión se cierra
		}
		
		cout << "- Mensaje del servidor: " << buffer << endl;
	}
	
	close(serverSocket);
	delete (int*)socketDesc;
	pthread_exit(NULL);
}

//* Función: Alineamiento de secuencias (BLAST)
// Funcion que implementa métodos de alineamiento de pares de segmentos máximos (BLAST)
void blastMethods(){
	// Variables locales
	int i{1}, flag, aux, integerOp{0}, BLASTn, BLASTp, databaseProtein, databaseNucleotide;;
	string valorOp{"\0"};
	string metodoAlignment{"\0"};

	cout << "\n ····· GENERANDO DATABASES. ····· "; 
	databaseProtein = system("makeblastdb -in P-modelOrganism.fasta -dbtype prot -out databaseProtein");
	databaseNucleotide = system("makeblastdb -in N-modelOrganism.fasta -dbtype nucl -out databaseNucleotide");
	sleep(1);

	while (i < 2){
		cout << "\n\n\t ---------------------------------------------------------- " << endl;
		cout << "\t ***** ¿Cómo desea alinear las secuencias ingresadas? ***** " << endl;
		cout << "\t ---------------------------------------------------------- " << endl;
		cout << "\n [1]  » Nucleotide BLAST (BLASTn, nucleotide → nucleotide)" << endl;
		cout << " [2]  » Protein BLAST (BLASTp, protein → protein)" << endl;
		cout << " [0]  » Salir del programa. " << endl;
		cout << "\n - Favor, elija una opción: " << endl << "  > ";

		cin >> valorOp;
		integerOp = atoi(valorOp.c_str());
		cin.ignore();

		if (integerOp == 1) {
			BLASTn = system("blastn -query seq1.fasta -db databaseNucleotide -out seq1_Resultados-BLASTn.txt");
			BLASTn = system("blastn -query seq2.fasta -db databaseNucleotide -out seq2_Resultados-BLASTn.txt");
			aux = system("gedit seq1_Resultados-BLASTn.txt");
			flag = system("gedit seq2_Resultados-BLASTn.txt");
			i++;
		} else if (integerOp == 2) {
			BLASTp = system("blastp -query seq1.fasta -db databaseProtein -out seq1_Resultados-BLASTp.txt");
			BLASTp = system("blastp -query seq2.fasta -db databaseProtein -out seq2_Resultados-BLASTp.txt");
			aux = system("gedit seq1_Resultados-BLASTp.txt");
			flag = system("gedit seq2_Resultados-BLASTp.txt");
			i++;
		} else if (integerOp == 0) {
			cout << "\033c";
			cout << "\t----------------------------" << endl;
			cout << "\t***** ¡SALIDA EXITOSA! *****" << endl;
			cout << "\t----------------------------\n" << endl;
			i++;
		} else {
			cout << "\033c";
			cerr << "\t--------------------------------------------------" << endl;
			cerr << "\t***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
			cerr << "\t--------------------------------------------------\n" << endl;	
		}
	}

}
//* Función: Alineamiento de secuencias (LOCAL/GLOBAL)
// Función que implementa métodos de alineamiento local/global.
void alignmentMethods(){
	// Variables locales
	int i{1}, integerOp{0}, bashScript, auxInvoc;
	string valorOp{"\0"};
	string metodoAlignment{"\0"};

	while (i <= 3){
		cout << "\n\n\t ---------------------------------------------------------- " << endl;
		cout << "\t ***** ¿Cómo desea alinear las secuencias ingresadas? ***** " << endl;
		cout << "\t ---------------------------------------------------------- " << endl;
		cout << "\n [1]  » Elegir método de alineamiento local (Smith-Waterman)" << endl;
		cout << " [2]  » Elegir método de alineamiento global (Needleman-Wunsch)" << endl;
		cout << " [3]  » Elegir método de alineamiento Pares de segmentos máximos (BLAST)" << endl;
		cout << " [0]  » Salir del programa. " << endl;
		cout << "\n - Favor, elija una opción: " << endl << "  > ";

		cin >> valorOp;
		integerOp = atoi(valorOp.c_str());
		cin.ignore();

		if (integerOp == 1) {
			bashScript = system("sh water.bash");
			auxInvoc = system("gedit sequences.water");
			i++;

		} else if (integerOp == 2) {
			bashScript = system("sh needle.bash");
			auxInvoc = system("gedit sequences.needle");
			i++;

		} else if (integerOp == 3) {
			blastMethods();

		} else if (integerOp == 0) {
			cout << "\033c";
			cout << "\t----------------------------" << endl;
			cout << "\t***** ¡SALIDA EXITOSA! *****" << endl;
			cout << "\t----------------------------\n" << endl;
			i++;

		} else {
			cout << "\033c";
			cout << "\t--------------------------------------------------" << endl;
			cout << "\t***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
			cout << "\t--------------------------------------------------\n" << endl;	
		}
	}
}


//* Función Main
// Función que representa al cliente, configurando el socket, dirección y conexión al servidor
int main(){
	// Variables locales
	int serverSocket;
	sockaddr_in serverAddr;
	
	// Crear el socket del cliente
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (serverSocket == -1) {
		cerr << "\t -------------------------------------------------- \n";
		cerr << "\t ***** ERROR: FALLO EN LA CREACIÓN DE SOCKET. ***** \n";
		cerr << "\t -------------------------------------------------- \n";
		exit(EXIT_FAILURE);
	}
	
	// Configurar la dirección del servidor
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	
	if (inet_pton(AF_INET, SERVER_IP, &(serverAddr.sin_addr)) <= 0) {
		cerr << "\t --------------------------------------------------- \n";
		cerr << "\t ***** ERROR: DIRECCIÓN INVÁLIDA DEL SERVIDOR. ***** \n";
		cerr << "\t --------------------------------------------------- \n";
		close(serverSocket);
		exit(EXIT_FAILURE);
	}
	
	// Conectar al servidor
	if (connect(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
		cerr << "\t ---------------------------------------------------- \n";
		cerr << "\t ***** ERROR: NO SE PUEDE CONECTAR AL SERVIDOR. ***** \n";
		cerr << "\t ---------------------------------------------------- \n";
		close(serverSocket);
		exit(EXIT_FAILURE);
	}

	cout << "\033c";
	cout << "\n\t ····· CONEXIÓN ESTABLECIDA CON EL SERVIDOR. ·····" << endl;
	
	// Crear un hilo para recibir mensajes del servidor
	if (pthread_create(&receiveThread, NULL, receiveMessages, (void*)&serverSocket) != 0) {
		cerr << "\t ------------------------------------------------- \n";
		cerr << "\t ***** ERROR: FALLO EN LA CREACIÓN DE HILOS. ***** \n";
		cerr << "\t ------------------------------------------------- \n";
		close(serverSocket);
		exit(EXIT_FAILURE);	
	}


	// Menú principal
	int i{1}, integerOp{0};
	string valorOp{"\0"};
	string idPSequence{"\0"};
	string idNSequence{"\0"};
	string proteinSequence{"\0"};
	string nucleotidSequence{"\0"};

	
	while (i < 2) {
		// Abrir archivos FASTA en modo de escritura.
		ofstream aseqFile("seq1.fasta");		
		ofstream bseqFile("seq2.fasta");

		cout << endl;
		cout << "\t ---------------------------- " << endl;
		cout << "\t ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t ---------------------------- " << endl;
		cout << "\n [1]  » Ingresar secuencias de aminoácidos (proteínas). " << endl;
		cout << " [2]  » Ingresar secuencias de nucleótidos (DNA/RNA). " << endl;		
		cout << " [0]  » Salir del programa. " << endl;
		cout << "\n - Favor, elija una opción: " << endl << "  > ";
		cin >> valorOp;
		integerOp = atoi(valorOp.c_str());
		cin.ignore();

		// Opciones
		if (integerOp == 1) {
			while (i <= 2){
				if (i == 1){
					cout << "\n Ingrese identificador de secuencia [" << i << "]" << endl << "  > ";
					getline(cin, idPSequence);
					aseqFile << ">" << idPSequence << endl;

					cout << " Ingrese la secuencia de aminoácidos (proteínas) [" << i << "]" << endl << "  > ";
					getline(cin, proteinSequence);
					aseqFile << proteinSequence << endl;
				} else if (i == 2) {
					cout << "\n Ingrese identificador de secuencia [" << i << "]" << endl << "  > ";
					getline(cin, idPSequence);
					bseqFile << ">" << idPSequence << endl;

					cout << " Ingrese la secuencia de aminoácidos (proteínas) [" << i << "]" << endl << "  > ";
					getline(cin, proteinSequence);
					bseqFile << proteinSequence << endl;
				}

				// Enviar la secuencia al servidor
				if (send(serverSocket, proteinSequence.c_str(), proteinSequence.length(), 0) == -1) {
					cerr << "\t--------------------------------------------------" << endl;
					cerr << "\t***** ERROR AL ENVIAR LA SECUENCIA BIOLÓGICA *****" << endl;
					cerr << "\t--------------------------------------------------" << endl;
				}
				i++;
			}
			alignmentMethods();		// Función de alineamiento de secuencias

		} else if (integerOp == 2) {
			while (i <= 2){
				if (i == 1){
					cout << "\n Ingrese identificador de secuencia [" << i << "]" << endl << "  > ";
					getline(cin, idNSequence);
					aseqFile << ">" << idNSequence << endl;

					cout << " Ingrese la secuencia de nucleótidos (DNA/RNA) [" << i << "]" << endl << "  > ";
					getline(cin, nucleotidSequence);
					aseqFile << nucleotidSequence << endl;
				} else if (i == 2) {
					cout << "\n Ingrese identificador de secuencia [" << i << "]" << endl << "  > ";
					getline(cin, idNSequence);
					bseqFile << ">" << idNSequence << endl;

					cout << " Ingrese la secuencia de nucleótidos (DNA/RNA) [" << i << "]" << endl << "  > ";
					getline(cin, nucleotidSequence);
					bseqFile << nucleotidSequence << endl;
				}

				// Enviar la secuencia al servidor
				if (send(serverSocket, nucleotidSequence.c_str(), nucleotidSequence.length(), 0) == -1) {
					cerr << "\t--------------------------------------------------" << endl;
					cerr << "\t***** ERROR AL ENVIAR LA SECUENCIA BIOLÓGICA *****" << endl;
					cerr << "\t--------------------------------------------------" << endl;
				}
				i++;
			}
			alignmentMethods();		// Función de alineamiento de secuencias

		} else if (integerOp == 0) {
			cout << "\033c";
			cout << "\t----------------------------" << endl;
			cout << "\t***** ¡SALIDA EXITOSA! *****" << endl;
			cout << "\t----------------------------\n" << endl;
			i++;
		}

		else {						// Manejo de opciones inválidas
			cout << "\033c";
			cerr << "\t--------------------------------------------------" << endl;
			cerr << "\t***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
			cerr << "\t--------------------------------------------------\n" << endl;
		} i += 1;
	}

	// Enviar la señal de finalización al servidor
	if (send(serverSocket, "FINISHED", strlen("FINISHED"), 0) == -1) {
		cerr << "\t--------------------------------------------------------" << endl;
		cerr << "\t***** ERROR AL ENVIAR SEÑAL DE TÉRMINO AL SERVIDOR *****" << endl;
		cerr << "\t--------------------------------------------------------\n" << endl;
	}
		
	// Cerrar la conexión y finalizar el programa de manera segura
	cout << "\n\n\t ····· CERRANDO EL PROGRAMA. ·····\n" << endl;
	
	pthread_cancel(receiveThread);  // Cancelar el hilo de recepción
	pthread_join(receiveThread, NULL);
	
	close(serverSocket);
	
	return 0;
}