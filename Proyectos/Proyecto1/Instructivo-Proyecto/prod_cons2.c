#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <unistd.h>

int buffer[5];

sem_t sem_prod;
sem_t sem_con;

void* productor(void* data)
{
        for(int i=0;i<50;i++){
                sem_wait(&sem_prod);
                buffer[i%50] = i;
                printf("Produciendo %d\n", i);
                sleep(2);
                sem_post(&sem_con);
        }
        pthread_exit(NULL);
}

void* consumidor(void* data)
{
        for(int i=0;i<50;i++){
                sem_wait(&sem_con);
                printf("Consumiendo %d\n",buffer[i%50]);
                sleep(2);
                fflush(NULL);
                sem_post(&sem_prod);
        }
        pthread_exit(NULL);
}


int main(int argc, char** argv)
{
        pthread_t prod;
        pthread_t con;

        sem_init(&sem_prod,0,5);
        sem_init(&sem_con,0,0);

        pthread_create(&prod,NULL,productor,NULL);
        pthread_create(&con,NULL,consumidor,NULL);

        pthread_join(con,NULL);
        return 0;
}