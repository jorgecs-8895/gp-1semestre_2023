#include <iostream>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <vector>
#include <string>

using namespace std;

////////////
class producto{
	public:
		int cuantos, max;

		vector<string> listaProductos;
	
		producto(){
			cuantos = 0;
		}

		void agregarItem(string p){
			if (cuantos < max){
	   		   listaProductos.insert(listaProductos.begin(),p);
			   cuantos++;
			}
		}

		string sacarItem(){
			cout << "sacando!\n";
			string prod = listaProductos.back();
			cout << "sacado " << prod << "\n";

			listaProductos.pop_back();
			cuantos--;
			return prod;
		}

		bool vacio(){
			if (cuantos==0)
				return true;
			return false;
		}
};
/////

producto p;
int NP, NC, CUANTOS_BUFFER, NPP, NCC;		 // Variables que almacenarán los parámetros anteriores como long int

pthread_mutex_t mutex_bufferCola;
sem_t sem_lleno;
sem_t sem_vacio;


void* Productor(void* arg) {
	int *id = ((int*) arg);

	cout << "Productor: " << *id << "\n";
    //cout << "NPP: " << NPP <<"\n";
	for (int i = 1; i <= NPP; i++) {
		sem_wait(&sem_vacio);
		pthread_mutex_lock(&mutex_bufferCola);

		p.agregarItem("Productor " + to_string(*id) + " produce " + to_string(i)); 		
		cout << "Productor " + to_string(*id) + " produce " + to_string(i) << " - Buffer " << p.cuantos <<"\n";
		
		pthread_mutex_unlock(&mutex_bufferCola);
		sem_post(&sem_lleno);
	}

	pthread_exit(NULL);
}


void* Consumidor(void* arg) {
	int *id = ((int*) arg);

   cout << "Consumidor: " << *id << "\n";

   for (int i = 1; i <= NCC; i++) {
   	    sem_wait(&sem_lleno);
   	    pthread_mutex_lock(&mutex_bufferCola);

		string valor = p.sacarItem();
		cout << "Consumidor " << *id << " consumed value " << valor << endl;
		
		pthread_mutex_unlock(&mutex_bufferCola);
		sem_post(&sem_vacio);
  }
  
  pthread_exit(NULL);
}

/////////////////////
int main(int argc, char *argv[]) {
	if (argc != 6) { 
		cout << "Debe Indicar 6 parámetros!\n";
		exit(1);
	}else {
		cout << "Ejecución Válida!\n";
		string cantProductor{argv[1]};	 
		string cantConsumidor{argv[2]}; 
		string tamannoBufer{argv[3]}; 
		string cuantosProductor{argv[4]};
		string cuantosConsumidor{argv[5]};	 


		// Conversión de tipos (int <- string)
		NP = atoi(cantProductor.c_str());
		NC = atoi(cantConsumidor.c_str());
		CUANTOS_BUFFER = atoi(tamannoBufer.c_str());
		NPP = atoi(cuantosProductor.c_str());
		NCC = atoi(cuantosConsumidor.c_str());

		cout << "NP: " << NP << " - NC: " << NC << " - Tamaño Búfer: " << CUANTOS_BUFFER << " - CP Y CC " << NCC << ", " << NPP << "\n";
		////Revisar si nadie muere de hambre...
		p.max = CUANTOS_BUFFER;
	}

	// Inicializar punteros, objetos y variables de manera local.
	pthread_t productor[NP];
	pthread_t consumidor[NC];
	
	pthread_mutex_init(&mutex_bufferCola, NULL);
	sem_init(&sem_lleno, 0, 0);
	sem_init(&sem_vacio, 0, CUANTOS_BUFFER);

	// Creación de hebras y control para cerciorarse de una correcta creación. (Productor)
	for (int i = 0; i < NP; i++) {
		pthread_create(&productor[i], NULL, &Productor, &i);
	}

	for (int i = 0; i < NC; i++) {
		pthread_create(&consumidor[i], NULL, &Consumidor, &i);
	}

	// Esperndo hebras y control para cerciorarse de una correcta creación. (Productor)
	for (int i = 0; i < NP; i++) {
		pthread_join(productor[i], NULL);
	}

	for (int i = 0; i < NC; i++) {
		pthread_join(consumidor[i], NULL);
	}

	cout << "Listo! \n";

	return 0;
}
