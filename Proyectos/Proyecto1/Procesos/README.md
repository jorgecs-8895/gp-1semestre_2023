# Proyecto 1 - Unidad 1

## Descripción
El siguiente proyecto fue escrito en C/C++, y consiste en un programa que lleva a cabo un problema del tipo Productor/Consumidor (ejemplo clásico de problema de sincronización de multiprocesos), cuya implementación se basa en el uso del concepto de Hebras (Pthreads - Threads), Semáforos y Exclusión Mutua.

## Historia
Escrito y desarrollado por los estudiantes de Ing. Civil en Bioinformática Jorge Carrillo y Cristóbal Briceño, utilizando para ello el lenguaje de programación C/C++.

## Para empezar
_Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos
Es requisito cerciorarse de tener instalado C/C++ y su compilador correspondiente en el equipo donde ejecutará el proyecto. Es recomendable que el SO de su máquina corra sobre el kernel Linux (Debian, Ubuntu, entre otras distribuciones).

### Instalación y ejecución
_Para ejecutar (sin instalar) el software:_

1. Descomprimir y extraer la carpeta llamada Proyecto1.
2. Entrar en la carpeta de interés llamada "Proyecto1/Procesos", que contiene el código fuente, programa, archivo de registro y MAKEFILE, además del archivo 'README.md' y el Instructivo de proyecto.
3. Abrir una terminal en la carpeta contenedora del programa.
4. Compilar el código fuente, ingresando para ello el siguiente comando en su terminal.

```
make
```

5. Para lanzar el programa, deberá ingresar el (los) siguiente(s) comando(s) en su terminal.

- Dentro de la carpeta de interés _Procesos_
```
./Procesos NP NC BC NPP NCC
```


## Construido con
* [Visual Studio Code.](https://code.visualstudio.com) - Principal IDE utilizado para el desarrollo del proyecto.
* [Vim.](https://www.vim.org/) - Editor de texto altamente configurable y eficiente.

## Codificación
Soporta la codificación estándar UTF-8.

## Autores️ y agradecimientos
* **Jorge Carrillo Silva.** - *Desarrollador - Programador* - [jorgecs-8895](https://gitlab.com/jorgecs-8895).
* **Cristóbal Briceño Arellano.** - *Desarrollador - Programador*

## Soporte
* **Jorge Carrillo Silva.** - *Desarrollador - Programador* - [jorgecs-8895](https://gitlab.com/jorgecs-8895). 
* E-mail: jcarrillo20@alumnos.utalca.cl

## Licencia
Este proyecto está sujeto bajo la Licencia GNU GPL v.3

## Estado del proyecto
Desarrollo activo hasta final de semestre (23-1).
