/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* INTEGRANTES: JORGE CARRILLO
 *              CRISTÓBAL BRICEÑO
 */

/******************** PROGRAMA.- ********************/

/*
 * file Main.cpp
 * g++ Main.cpp -pthread -o Procesos
 * make Main.cpp
 *      Main.o
 *      Procesos
 */

//* Main.cpp - Programa principal.

// Llamar e incluir librerías necesarias.
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <pthread.h>
#include <random>
#include <semaphore.h>
#include <unistd.h>

using namespace std;

// Prototipos de funciones.
void* funcProductores(void*);
void* funcConsumidores(void*);

// Inicializar mutex y semáforos.
pthread_mutex_t mutex_bufferCola;
sem_t sem_vacio, sem_lleno;

// Abrir archivo de registro en modo de escritura.
ofstream logfile("Procesos_ArchivoRegistro.log");

// Declaración de variables globales y arreglo circular FIFO (buffer/HEAD/TAIL/SIZE).
int NP, NC, AB, NPP, NCC;		 // Variables que almacenarán los valores ingresados por parámetros como int.
int* buffer;					 // Arreglo de puntero(s).
int HEAD{0};
int TAIL{0};
int SIZE{0};


//* ----- CUERPO DEL CÓDIGO ----- *//

//* Función productor.
// Función que representa al proceso "Productor". Genera un elemento en el buffer a la vez que bloquea acceso y agrega el item.
void* funcProductores(void* arg) {
	int id = *((int*)arg);

	// Generador de números aleatorios para el tiempo que debe "dormir" el programa, y para el elemento a insertar/eliminar del buffer
	random_device numAleatorio;
	mt19937 gen(numAleatorio());
	uniform_int_distribution<> duerme(1,5);
	uniform_int_distribution<> cadenaDatos(1,99);

	for (int i = 1; i <= NP; i++) {
		// El productor espera entre 1 y 5 segundos antes de producir un ítem.
		int tiempoEspera1 = duerme(gen);
		sleep(tiempoEspera1);

		// El productor genera un item (valor aleatorio) y lo ingresa en el buffer, utilizando una combinación de un semáforo y un mutex para garantizar que no se produzcan problemas de concurrencia.
		int item = cadenaDatos(gen);
		sem_wait(&sem_vacio);
		pthread_mutex_lock(&mutex_bufferCola);
		
		// Ingresa entrada en el archivo de registro
		logfile << "\n» Productor nro [" << id << "] | INTENTANDO INGRESAR EL ELEMENTO [" << item << "] AL BUFFER." << endl;
		
		// Si hay suficiente espacio disponible en el buffer, el elemento se agrega en la posición indicada por la variable "TAIL", considerando siempre el espacio limitado AB del buffer.
		if (SIZE < AB) {
			buffer[TAIL] = item;
			TAIL = (TAIL + 1) % AB;
			SIZE++;

			// Imprimir y registrar producción de cada hebra Productor
			cout << "» Productor nro [" << id << "] - Generó [" << item << "]" << endl;
			// logfile << "» Productor nro [" << id << "] - Generó [" << item << "]" << endl;
			logfile << "» Productor nro [" << id << "] - Generó [" << item << "] | INSERCIÓN EXITOSA. " << endl;
		} else {
			// Si el buffer está lleno, imprimir y registrar entrada correspondiente en el archivo de registro.
			cout << "» Productor nro [" << id << "] | ERROR DE INSERCIÓN - BUFFER LLENO" << endl;
			logfile << "» Productor nro [" << id << "] | ERROR DE INSERCIÓN - BUFFER LLENO" << endl;

			// Esperar una cantidad de tiempo aleatoria antes de intentar nuevamente insertar el mismo elemento
			int tiempoEspera2 = duerme(gen);
			sleep(tiempoEspera2);
		}
		// Se utiliza la función mutex "pthread_mutex_unlock" para liberar el acceso al buffer y la función "sem_post" para incrementar el valor del semáforo "sem_lleno"
		pthread_mutex_unlock(&mutex_bufferCola);
		sem_post(&sem_lleno);
	}
	cout << endl;
	
	// Finaliza la ejecución de la hebra Productor <id> en cuestión.
	pthread_exit(NULL);
}

//* Función consumidor.
// Función que representa al proceso "Consumidor". Evalúa si hay espacios disponibles en buffer, si los hubiera, bloquea su acceso (pthread_mutex_unlock) y chequea otra vez los espacios en buffer.
void* funcConsumidores(void* arg) {
	int id = *((int*)arg);

	for (int i = 1; i <= NC; i++) {
		// La hebra espera a que el semáforo sem_lleno tenga un valor mayor que 0, lo que indica que hay al menos un elemento en el búfer que se puede consumir.
		sem_wait(&sem_lleno);
		pthread_mutex_lock(&mutex_bufferCola);

		// Ingresa entrada en el archivo de registro
		logfile << "~ Consumidor nro [" << id << "] | INTENTANDO ELIMINAR EL ELEMENTO DEL BUFFER." << endl;
		
		// Si el tamaño del búfer es mayor que 0, el consumidor elimina un elemento del búfer y lo consume.
		if (SIZE > 0) {
			int item = buffer[HEAD];
			HEAD = (HEAD + 1) % AB;
			SIZE--;

			// Imprimir y registrar consumo de cada hebra Consumidor
			cout << "~ Consumidor nro [" << id << "] - Consumió [" << item << "]\n" << endl;
			// logfile << "~ Consumidor nro [" << id << "] - Consumió [" << item << "]" << endl;
			logfile << "~ Consumidor nro [" << id << "] - Consumió [" << item << "] | ELIMINADO CON ÉXITO. " << "\n" << endl;
		} else {
			// Si el tamaño del búfer es 0, el consumidor no puede consumir nada..
			cout << "~ Consumidor nro [" << id << "] - No consumió. \n" << endl;
			logfile << "~ Consumidor nro [" << id << "] - No consumió. \n" << endl;
		}
		// Se utiliza la función mutex "pthread_mutex_unlock" para liberar el acceso al buffer y la función "sem_post" para incrementar el valor del semáforo "sem_vacio"
		pthread_mutex_unlock(&mutex_bufferCola);
		sem_post(&sem_vacio);
	}
	cout << endl;
	
	// Finaliza la ejecución de la hebra Consumidor <id> en cuestión.
	pthread_exit(NULL);
}

//* Función Main.
// Función que recibe los parámetros ingresados por el usuario, los evalúa, y si cumplen las condiciones, genera semáforos, mutex y NP/NC hebras.
int main(int argc, char *argv[]) {
	cout << "\033c";

	// Analizar los argumentos de la línea de comandos ingresados por el usuario.
	if (argc != 6) { // Si el programa no recibe parámetros válidos (el nombre de archivo + 5 argumentos de la línea de comando), entonces:
		cerr << "\t ----------------------------------------------------------------------------------- \n";
		cerr << "\t ***** ERROR: PARÁMETROS INSUFICIENTES Y/O INVÁLIDOS. FAVOR INTENTE NUEVAMENTE ***** \n";
		cerr << "\t ----------------------------------------------------------------------------------- \n\n";
		exit(1);
	}

	// Almacenar los valores pasados por parámetros y posterior conversión de tipos de datos.
	string cant_procProductor{argv[1]};	 // Guarda el valor del 2do parámetro (Número de procesos productores (en adelante representados como NP)).
	string cant_procConsumidor{argv[2]}; // Guarda el valor del 3er parámetro (Número de procesos consumidores (en adelante representados como NC)).
	string almacenamientoBufer{argv[3]}; // Guarda el valor del 4to parámetro (Capacidad de almacenamiento del buffer (AB)).
	string produccionProductor{argv[4]}; // Guarda el valor del 5to parámetro (Número de veces que un proceso productor producirá (NPP)).
	string consumoConsumidor{argv[5]};	 // Guarda el valor del 6to parámetro (Número de veces que un proceso consumidor consumirá (NCC)).

	// Conversión de tipos (int <- string), mediante control de excepciones.
	try {
		NP = stoi(cant_procProductor);
		NC = stoi(cant_procConsumidor);
		AB = stoi(almacenamientoBufer);
		NPP = stoi(produccionProductor);
		NCC = stoi(consumoConsumidor);
	} catch (exception const& e) {
		cerr << "\t ----------------------------------------------------------------------- \n";
		cerr << "\t ***** ERROR: UNO DE LOS ARGUMENTOS NO ES UN NÚMERO ENTERO VÁLIDO. ***** \n";
		cerr << "\t ----------------------------------------------------------------------- \n\n";
		exit(1);
	}

	// Verificar que los valores ingresados por el usuario sean mayores que cero.
	if ((NP <= 0) || (NC <= 0) || (AB <= 0) || (NPP <= 0) || (NCC <= 0)) {
		cerr << "\t ------------------------------------------------------------------------------------------ \n";
		cerr << "\t ***** ERROR: TODOS LOS ARGUMENTOS DEBEN SER NÚMEROS ENTEROS POSITIVOS (MAYOR QUE 0). ***** \n";
		cerr << "\t ------------------------------------------------------------------------------------------ \n\n";
		exit(1);
	}

	// Multiplica la cantidad de procesos por la cantidad de producción/consumo correspondiente.
	auto multProductor = NP * NPP;
	auto multConsumidor = NC * NCC;

	if ((NPP  < NCC) || (multProductor < multConsumidor) || (AB < NP + NC)) {	// Se genera condicional para evaluar si las condiciones se satisfacen (que el tamaño del buffer NO sea menor a la cantidad de productores/consumidores y que no puede haber mas consumo que producción).
		cerr << "\t ----------------------------------------------------------------------------------------------------------------- \n";
		cerr << "\t ***** ERROR: HAY MÁS CONSUMO QUE PRODUCCIÓN O EL ALMACENAMIENTO DEL BUFFER ES MENOR AL VALOR ENTRE NP Y NC. ***** \n";
		cerr << "\t ----------------------------------------------------------------------------------------------------------------- \n\n";
		exit(1);
	}

	// Impresión de pantalla con presentación de valores ingresados por el usuario
	cout << "\t***** PROBLEMA PRODUCTOR/CONSUMIDOR *****\n" << endl;
	cout << "· Número de procesos productores (NP): " << NP << endl;
	cout << "· Número de procesos consumidores (NC): " << NC << endl;
	cout << "· Capacidad de almacenamiento del buffer (AB): " << AB << endl;
	cout << "· Número de veces que un proceso productor producirá (NPP): " << NPP << endl;
	cout << "· Número de veces que un proceso consumidor consumirá (NCC): " << NCC << endl << endl;

	// Primeras entradas en cabecera de archivo de registro.
	logfile << "\t***** ABRE ARCHIVO DE REGISTRO: PROBLEMA PRODUCTOR/CONSUMIDOR *****\n" << endl;
	logfile << "· Número de procesos productores (NP): " << NP << endl;
	logfile << "· Número de procesos consumidores (NC): " << NC << endl;
	logfile << "· Capacidad de almacenamiento del buffer (AB): " << AB << endl;
	logfile << "· Número de veces que un proceso productor producirá (NPP): " << NPP << endl;
	logfile << "· Número de veces que un proceso consumidor consumirá (NCC): " << NPP << endl << endl;
	
	// Crear semáforos y mutex.
	pthread_mutex_init(&mutex_bufferCola, NULL);
	sem_init(&sem_vacio, 0, AB);
	sem_init(&sem_lleno, 0, 0);

	// Asignar espacio en memoria con malloc (heredado de C).
	buffer = (int*)malloc(AB * sizeof(int));

	// Generar hebras y arreglos.
	pthread_t hebraProductores[NP], hebraConsumidores[NC];
	int productoresID[NP], consumidoresID[NC];

	// Ciclos de generación de hebras y control para asegurar una correcta creación (Productor).
	for (int i = 1; i <= NP; i++) {
		productoresID[i] = i;
		int controlHebraNP = pthread_create(&hebraProductores[i], NULL, &funcProductores, &productoresID[i]);
	
		if ((controlHebraNP) != 0) { // Si el proceso de creación de hilos no fue exitoso, lanza mensaje de advertencia.
			cerr << "\t --------------------------------------------------- \n";
			cerr << "\t ***** ERROR: FALLO EN LA CREACIÓN DE HILOS. ***** \n";
			cerr << "\t --------------------------------------------------- \n";
			exit(EXIT_FAILURE);
		}
		// Imprimir y registrar cada creación de hebras (Productor).
		cout << "> Productor: " << i << " creado." << endl;
		logfile << "> Productor: " << i << " creado." << endl;
	}

	// Ciclos de generación de hebras y control para asegurar una correcta creación (Consumidor).
	for (int i = 1; i <= NC; i++) {
		consumidoresID[i] = i;
		int controlHebraNC = pthread_create(&hebraConsumidores[i], NULL, &funcConsumidores, &consumidoresID[i]);

		if ((controlHebraNC) != 0) { // Si el proceso de creación de hilos no fue exitoso, lanza mensaje de advertencia.
			cerr << "\t --------------------------------------------------- \n";
			cerr << "\t ***** ERROR: FALLO EN LA CREACIÓN DE HILOS. ***** \n";
			cerr << "\t --------------------------------------------------- \n";
			exit(EXIT_FAILURE);
		}
		// Imprimir y registrar cada creación de hebras (Consumidor).
		cout << "─ Consumidor: " << i << " creado." << endl;
		logfile << "─ Consumidor: " << i << " creado." << endl;
	}

	cout << "\n";

	// Ciclo que debe esperar a que cada hilo (Productor) termine su proceso y lo registra.
	for (int i = 1; i <= NP; i++) {
		pthread_join(hebraProductores[i], NULL);
		cout << "> Productor: " << i << " terminado." << endl;
		logfile << "> Productor: " << i << " terminado." << endl;
	}

	// Ciclo que debe esperar a que cada hilo (Consumidor) termine su proceso y lo registra.
	for (int i = 1; i <= NC; i++) {
		pthread_join(hebraConsumidores[i], NULL);
		cout << "─ Consumidor: " << i << " terminado." << endl;
		logfile << "─ Consumidor: " << i << " terminado." << endl;
	}

	// Última impresión de pantalla y entrada en el archivo de registro.
	cout << "\n\t***** CIERRE DE PROGRAMA *****" << endl;
	logfile << "\n\t***** CIERRE ARCHIVO DE REGISTRO: PROBLEMA PRODUCTOR/CONSUMIDOR *****" << endl;

	// Cierre de archivo de registro y destrucción de semáforos y hebras.
	logfile.close();
	free(buffer);
	sem_destroy(&sem_vacio);
	sem_destroy(&sem_lleno);
	pthread_mutex_destroy(&mutex_bufferCola);

	return 0;
}
