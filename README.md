# GP-1Semestre_2023

## Historia 
Desarrollado por el estudiante de Ingeniería Civil en Bioinformática, Jorge Carrillo Silva, las siguientes carpetas (Guias - Proyectos) contienen los distintos códigos fuentes, aplicando los contenidos y paradigmas vistos en clase de Sistemas Operativos y Redes, utilizando para ello el lenguaje de programación C/C++.

## Descripción
En la carpeta _Guías_, pueden encontrar los códigos fuentes, clasificados por unidades temáticas, donde podemos ver la implementación del concept de Programación Concurrente, pasando desde Procesos, hebras POSIX, hasta semáforos y exclusión mutua.
La carpeta _Proyectos_ también se separa por unidad temática, y en esencia, corresponde a la implementación de todo lo visto en la Unidad en un solo proyecto.

## Para empezar
_Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos
Es requisito cerciorarse de tener instalado C/C++ y su compilador correspondiente en el equipo donde ejecutará el proyecto. Es recomendable que el SO de su máquina corra sobre el kernel Linux (Debian, Ubuntu, entre otras distribuciones).

### Instalación y ejecución
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

_Para ejecutar (sin instalar) el software:_

1. Descomprimir y extraer la carpeta en cuestión.
2. Entrar en la carpeta de interés, que contiene el código fuente, programa, archivo de registro y MAKEFILE, además del archivo 'README.md' y el Instructivo de proyecto.
3. Abrir una terminal en la carpeta contenedora del programa.
4. Compilar el código fuente, ingresando para ello el siguiente comando en su terminal.

```
make
```

5. Para lanzar el programa, deberá ingresar el (los) siguiente(s) comando(s) en su terminal (Esto es una generalización de la ejecución de la mayoría de programas y puede cambiar según sea necesario).

- Dentro de la carpeta de interés
```
./NombrePrograma
```

## Codificación
Soporta la codificación estándar UTF-8.


## Construido con
* [Visual Studio Code.](https://code.visualstudio.com) - Principal IDE utilizado para el desarrollo del proyecto.
* [Vim.](https://www.vim.org/) - Editor de texto altamente configurable y eficiente.


## Soporte
* **Jorge Carrillo Silva.** - *Desarrollador - Programador* - [jorgecs-8895](https://gitlab.com/jorgecs-8895).
E-mail: jcarrillo20@alumnos.utalca.cl

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contribuciones
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Autores y agradecimientos
* **Jorge Carrillo Silva.** - *Desarrollador - Programador* - [jorgecs-8895](https://gitlab.com/jorgecs-8895).

## Licencia
Todos los proyectos están sujetos bajo la Licencia GNU GPL v.3.

## Estado del proyecto
Desarrollo activo hasta final de semestre (23-1).
