#include <stdio.h>
#include <pthread.h>
        
/*Definición de Función de la Hebra*/
void* funcionHebra(void* args){
    while(1){
        printf("Yo soy la Hebra Hija!\n");
    }
}

int main(){

    /*Creación del id de la hebra*/
    pthread_t id;
    int ret;
    
    /*Creación de la hebra*/
    ret = pthread_create(&id, NULL, &funcionHebra, NULL);

    if (ret == 0){
        printf("Hebra hija creada exitosamente!\n");
    }
    else{
        printf("Hebra hija no puedo ser creada!\n");
        return 0; 
    }
    
    while(1){
        printf("Yo soy la Hebra Principal!\n");      
    }
    
    return 0;
}