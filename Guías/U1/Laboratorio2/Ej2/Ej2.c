/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* INTEGRANTES: JORGE CARRILLO
                CRISTÓBAL BRICEÑO */

/******************** PROGRAMA.- ********************/

/*
 * file Ej2.c
 * gcc -g -Wall -pthread -c -o Ej2.o Ej2.c
 * make Ej2.c
 *      Ej2.o
 *      Ejercicio2
*/

//* Ejercicio2.c - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#define _OPEN_THREADS
#define nCicloPadre 1 // Se genera valor constante para ciclo de generación de hebra padre
#define nCicloHijas 3 // Se genera valor constante para ciclo de generación de hebras hijas

/* Cuerpo del código */
void *generarHebraHijas(void *arg){
    printf("> Se genera hebra HIJA n° #%ld\n", (long)arg); // Imprime generación de hebras
    printf(" H%ld Creado! \n\n", (long)arg);
    pthread_exit(NULL);                             // Termina cada hilo correspondiente
}

void *crearHebras(void *arg) { /* Función apuntador que recibe argumento de creación de hebras de la función main*/
    printf("\t-- Se genera hebra Padre n° #%ld --\n", (long)arg); // Imprime generación de hebras
    pthread_exit(NULL);                             // Termina cada hilo correspondiente
}

int main() { /* Función que define la creación de hilos y variables a ocupar*/
    // Variables e identificador de hilos
    pthread_t hebraPadre[nCicloPadre], hebraHijas[nCicloHijas]; ;
    long controlHebraPadre, controlHebrasHijas;

    controlHebraPadre = pthread_create(&hebraPadre[nCicloPadre], NULL, &crearHebras, (void*)nCicloPadre);

    for(long contadorHijas = 1; contadorHijas <= nCicloHijas; contadorHijas++) { // Ciclo que genera 3 hebras, con la variable constante declarada previamente, y pasa argumento a un termino nuevo
        controlHebrasHijas = pthread_create(&hebraHijas[nCicloHijas], NULL, &generarHebraHijas, (void*)contadorHijas);

        if((controlHebraPadre != 0) && (controlHebrasHijas != 0)) { // Si el proceso de creación de hilos no fue exitoso, lanza mensaje de advertencia
            printf("\tERROR, FALLO EN LA CREACIÓN DE HILOS\n");
            exit(EXIT_FAILURE);
        }
    }

    pthread_join(controlHebraPadre, NULL);
    pthread_join(controlHebrasHijas, NULL); 
    pthread_exit(NULL);

    return 0;
}
