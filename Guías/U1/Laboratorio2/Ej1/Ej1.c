/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* INTEGRANTES: JORGE CARRILLO
                CRISTÓBAL BRICEÑO */

/******************** PROGRAMA.- ********************/

/*
 * file Ej1.c
 * gcc -g -Wall -pthread -c -o Ej1.o Ej1.c
 * make Ej1.c
 *      Ej1.o
 *      Ejercicio1
*/

//* Ejercicio1.c - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#define _OPEN_THREADS
#define nCiclos 8 // Se genera valor constante para ciclo de generación de hebras

/* Cuerpo del Código */
void *crearHebras(void *arg) { /* Función apuntador que recibe argumento de creación de hebras de la función main*/
    printf("Se genera hebra n° #%ld\n", (long)arg); // Imprime generación de hebras
    pthread_exit(NULL);                             // Termina cada hilo correspondiente
}

int main() { /* Función que define la creación de hilos y variables a ocupar*/
    // Variables e identificador de hilos
    pthread_t cantHebras[nCiclos];
    long i, controlHebras;

    for(i = 1; i <= nCiclos; i++) { // Ciclo que genera 8 hebras, con la variable constante declarada previamente, y pasa argumento a un termino nuevo
        controlHebras = pthread_create(&cantHebras[nCiclos], NULL, &crearHebras, (void*)i);

        if(controlHebras != 0) { // Si el proceso de creación de hilos no fue exitoso, lanza mensaje de advertencia
            printf("\tERROR, FALLO EN LA CREACIÓN DE HILOS\n");
            exit(EXIT_FAILURE);
        }
    }

    pthread_join(controlHebras, NULL);
    pthread_exit(NULL);

    return 0;
}