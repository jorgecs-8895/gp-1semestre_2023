/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* INTEGRANTES: JORGE CARRILLO
                CRISTÓBAL BRICEÑO */

/******************** PROGRAMA.- ********************/

/*
 * file Ej3.c
 * gcc -g -Wall -pthread -c -o Ej3.o Ej3.c
 * make Ej3.c
 *      Ej3.o
 *      Ejercicio3
*/

//* Ejercicio3.c - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#define _OPEN_THREADS
#define nCicloHijas 1 // Se genera valor constante para ciclo de generación de hebra padre

/* Cuerpo del código */
void* generarHebraNieta(void* arg){
    // Imprime generación de hebras
    printf("\n\t-- Se genera la hebra sobrina --\n");
    printf("> H3 Creado!\n");

    // Imprime termino de hebras
    printf("\n+ H3 Termina!");
    printf("\n+ H2 Termina!");
    printf("\n+ H1 Termina!\n");
    pthread_exit(NULL);
}

void* generarHebraHija(void* arg){ // Función que genera hebra nieta en sincronización con Hija
    // Variables e identificador de hilos
    pthread_t hebraNietas;
    long controlHebraHijas;

    // Si el proceso de creación de hilos no fue exitoso, lanza mensaje de advertencia    controlHebraHijas = pthread_create(&hebraNietas, NULL, generarHebraNieta, NULL);    
    if(controlHebraHijas != 0) {
        printf("\t--- ERROR, FALLO EN LA CREACIÓN DE HILOS. ---\n");
        exit(EXIT_FAILURE);
    }

    // Imprime generación de hebras
    printf("\n\t-- Se genera hebra hija --\n");
    printf("> H2 Creado!\n");

    pthread_join(hebraNietas, NULL);
    pthread_exit(NULL);
}

int main(){ /* Función que define la creación de hilos y variables a ocupar*/
    // Variables e identificador de hilos
    pthread_t hebraHijas;
    long controlHebraPadre;

    controlHebraPadre = pthread_create(&hebraHijas, NULL, &generarHebraHija, (void*)nCicloHijas);

    if (controlHebraPadre != 0) { // Si el proceso de creación de hilos no fue exitoso, lanza mensaje de advertencia
        printf("\t--- ERROR, FALLO EN LA CREACIÓN DE HILOS. ---\n");
        exit(EXIT_FAILURE);
    }

    // Imprime generación de hebras
    printf("\t-- Se genera hebra Padre --\n");
    printf("> H1 Creado!\n");

    pthread_join(hebraHijas, NULL);
    pthread_exit(NULL);

    return 0;
}

