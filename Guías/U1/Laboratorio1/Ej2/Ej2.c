/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* INTEGRANTES: JORGE CARRILLO
                CRISTÓBAL BRICEÑO */

/******************** PROGRAMA.- ********************/

/*
 * file Ej2.cpp
 * gcc Ej2.cpp -o Ej2
 * make Ej2.cpp
 *      Ej2.o
 *      Ejercicio2
*/

//* Ejercicio2.cpp - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


//Incluir función principal
int main(int argc, char *argv[]){

    //printf("\033c"); // Limpiar pantalla
    
    /* Inicializacion de variables. */

    int varEspera1, varEspera2, varEspera3; // variables auxiliares de waitpid
    pid_t procesoHijo1, procesoHijo2, procesoHijo3; // Identificador de procesos.


    /* Cuerpo del codigo*/

    // En esta porción de código, utilizamos fork por cada proceso hijo,
    // además de establecer condicionales para poder ejecutar el resto de instrucciones.

    if ((procesoHijo1 = fork()) == 0){   // Si la variable es 0, entonces desarrollará las instrucciones correspondientes al proceso hijo1
        printf("\t H1 Creado!\n");
        printf("Soy proceso hijo 1 . Mi ID es %d. \n", getpid());
        return(0);
    } else {                   // Si la variable es != 0, entonces desarrollará las instrucciones correspondientes al proceso Hijo2, Hijo3 y Padre
        if ((procesoHijo2 = fork()) == 0){   // Si la variable es 0, entonces desarrollará las instrucciones correspondientes al proceso hijo2
            printf("\n\t H2 Creado!\n");
            printf("Soy proceso hijo 2. Mi ID es %d. \n", getpid());
            return(0);
        } else {                   // Si la variable es != 0, entonces desarrollará las instrucciones Hijo3 - padre
            if ((procesoHijo3 = fork()) == 0){
                printf("\n\t H3 Creado!\n");
                printf("Soy proceso hijo 3. Mi ID es %d. \n", getpid());
                return(0);
            } else {
                
                // Función de espera por el final de los procesos hijos antes de ejecutar padre
                waitpid(procesoHijo1, &varEspera1, 0);
                waitpid(procesoHijo2, &varEspera2, 0);
                waitpid(procesoHijo3, &varEspera3, 0);

                // Impresión de proceso padre, post ejecución de procesos hijos
                printf("\n\t Proceso Padre\n");
                printf("Soy proceso padre. Mi ID es %d. \n", getpid());
                return(0);
            }
        }
    return 0;
    }
}