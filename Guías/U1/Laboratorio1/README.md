# Guía 1 - Unidad 1

Los siguientes ejercicios fueron escritos en C, y consisten en una serie de programas cuya implementación se basa en el uso del concepto de Procesos.


## Historia

Escrito y desarrollado por los estudiantes de Ing. Civil en Bioinformática Jorge Carrillo y Cristóbal Briceño, utilizando para ello el lenguaje de programación C.


## Para empezar

_Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos

Es requisito cerciorarse de tener instalado C y su compilador correspondiente en el equipo donde ejecutará el proyecto. Es recomendable que el SO de su máquina corra sobre el kernel Linux (Debian, Ubuntu, entre otras distribuciones).


### Instalación y ejecución

_Para ejecutar (sin instalar) el software:_

1. Descomprimir y extraer la carpeta llamada Guía1.
2. Entrar en la carpeta de interés llamadas "Guia1/Ej1 - Ej2 - Ej3", que contienen los códigos fuentes y programas, además del archivo 'README.md' y el Instructivo de proyecto.
3. Abrir una terminal en la carpeta contenedora del programa.
4. Compilar el código fuente, ingresando para ello el siguiente comando en su terminal.

```
make
```

5. Para lanzar el programa, deberá ingresar el (los) siguiente(s) comando(s) en su terminal.

- Dentro de la carpeta de interés _Ej1 - Ej2 - Ej3_
```
./Ejercicio1.
```
```
./Ejercicio2.
```
```
./Ejercicio3.
```


## Codificación

Soporta la codificación estándar UTF-8.


## Construido con

* [Visual Studio Code.](https://code.visualstudio.com) - IDE utilizado para el desarrollo del proyecto.


## Autores️

* **Jorge Carrillo Silva.** - *Desarrollador - Programador* - [jorgecs-8895](https://gitlab.com/jorgecs-8895).
* **Cristóbal Briceño.** - *Desarrollador - Programador*


## Licencia

Este proyecto está sujeto bajo la Licencia (GNU GPL v.3).
