/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* INTEGRANTES: JORGE CARRILLO
                CRISTÓBAL BRICEÑO */

/******************** PROGRAMA.- ********************/

/*
 * file Ej1.cpp
 * gcc Ej1.cpp -o Ej1
 * make Ej1.cpp
 *      Ej1.o
 *      Ejercicio1
*/

//* Ejercicio1.cpp - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define cantProcesos 7 // Por los 7 procesos hijos, además del padre (8 en total).

//Incluir función principal
int main(int argc, char *argv[]){

    //printf("\033c"); // Limpiar pantalla
    /* Inicializacion de variables. */

    int numeroProcesos = 0;
    pid_t procesoHijos; // Identificador de procesos.


    /* Cuerpo del codigo*/

    while (numeroProcesos < cantProcesos){ // Ciclo de repeticion, generación de procesos y control de errores, si los hubiera.
        procesoHijos = fork();

        if (procesoHijos == 0){         // Condicional de verificacion de creacion correcta de procesos
            break;
        } else if (procesoHijos == -1){
            printf("Error al crear proceso con fork()\n");
            exit(1);
            break;
        } numeroProcesos++;
    }

    if (procesoHijos == 0){   // Si la variable es 0, entonces desarrollará las instrucciones correspondientes al proceso hijo
        printf("\n> Soy proceso hijo. Mi ID es %d. \n", getpid());
        return(0);
    } else {                   // Si la variable es != 0, entonces desarrollará las instrucciones correspondientes al proceso padre
        printf("\n> Soy proceso padre. Mi ID es %d. \n", getpid());
        return(0);
    }

    return 0;
}