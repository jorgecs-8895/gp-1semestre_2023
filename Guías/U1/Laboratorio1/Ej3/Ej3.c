/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* INTEGRANTES: JORGE CARRILLO
                CRISTÓBAL BRICEÑO */

/******************** PROGRAMA.- ********************/

/*
 * file Ej3.cpp
 * gcc Ej3.cpp -o Ej3
 * make Ej3.cpp
 *      Ej3.o
 *      Ejercicio3
*/

//* Ejercicio3.cpp - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


//Incluir función principal
int main(int argc, char *argv[]){

    //printf("\033c"); // Limpiar pantalla

    /* Inicializacion de variables. */
    pid_t procesoHijo; // A partir de este pid_t, armaremos los siguientes procesos


    /* Cuerpo del codigo*/

    // En esta porción de código, utilizamos fork por cada proceso padre, hijo y nieto,
    // además de establecer condicionales para poder ejecutar el resto de instrucciones.
    if ((procesoHijo = fork()) == 0){   // Si la variable es 0, entonces desarrollará las instrucciones correspondientes al proceso Hijo
        printf("\n\t H1 Creado!\n");
        printf("Soy proceso Hijo . Mi ID es %d. \n", getpid());
        printf("\t H1 Terminado!\n");

        pid_t procesoNieto; // Crear proceso dentro de H1 (Hijo -> Nieto)

        if ((procesoNieto = fork()) == 0){   // Si la variable es 0, entonces desarrollará las instrucciones correspondientes al proceso Nieto
            printf("\n\t H2 Creado!\n");
            printf("Soy proceso Nieto. Mi ID es %d. \n", getpid());
            printf("\t H2 Terminado!\n");

            pid_t procesoBisnieto; // Crear proceso dentro de H2 (Nieto -> Bisnieto)
            
            //return(0);
            if ((procesoBisnieto = fork()) == 0){   // Si la variable es 0, entonces desarrollará las instrucciones correspondientes al proceso Bisnieto
                printf("\n\t H3 Creado!\n");
                printf("Soy proceso Bisnieto. Mi ID es %d. \n", getpid());
                printf("\t H3 Terminado!\n");
                exit(0);
            }
        }
    } else { // De otra manera, se imprime el proceso padre.
        printf("\t Proceso Padre\n");
        printf("Soy proceso padre. Mi ID es %d. \n", getpid());
        return(0);
    }
}