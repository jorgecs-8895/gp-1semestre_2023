/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* INTEGRANTES: JORGE CARRILLO
                CRISTÓBAL BRICEÑO */

/******************** PROGRAMA.- ********************/

/*
 * file Ej1.c
 * gcc -g -Wall -pthread -o Ejercicio1 Ej1.o
 * make Ej1.c
 *      Ej1.o
 *      Ejercicio1
*/

//* Ej1.c - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>  // Librería de hilos.
#include <semaphore.h> // Se añade librería que integra semáforos y mutex (exclusión mutua).

#define _OPEN_THREADS
#define nHebras 5 // Se genera valor constante para cantidad de generación de hebras

pthread_mutex_t sem_mutex;


/* Cuerpo del código */
void* imprimirContenido(void* arg) {  // Única función y que imprime el hilo y su ciclo correspondiente, respectivamente.
    // Declaración de variables y de espera de término de proceso de hilo, usando mutex SIN SEMAFOROS
    int idHilos = (int)arg; 
    int inicio = idHilos * 5 + 1;
    int final = inicio + (4);

    for (int imp = inicio; imp <= final; imp++) { // Ciclo que imprime los hilos y su número de impresiones.
        pthread_mutex_lock(&sem_mutex);  // se bloquea el mutex y por consiguiente, el subproceso de llamada que le sigue
        printf("Hilo %d impresiones %d\n", idHilos, imp);
        pthread_mutex_unlock(&sem_mutex);  // se desbloquea el mutex y por consiguiente, el subproceso de llamada que le sigue
    } // printf("\n"); // Salto de linea (mejora la visualización de la impresión)

    // sem_post(&sem_mutex); // Liberar el semáforo
    pthread_exit(NULL);
}

/* FUNCIÓN PRINCIPAL */
int main() {
    // Declaración de mutex (Exclusión mutua), hilos y variables locales
//    sem_init(&sem_mutex, 0, 1);
    pthread_t threads[nHebras];
    long i, controlHebras;

    // Inicializar el mutex
    pthread_mutex_init(&sem_mutex, NULL);

    // Ciclo de control y generación de hebras.
    for (i = 0; i < nHebras; i++) {
        controlHebras = pthread_create(&threads[i], NULL, &imprimirContenido, (void*)i);
        
        if(controlHebras != 0) { // Si el proceso de creación de hilos no fue exitoso, lanza mensaje de advertencia
            printf("\tERROR, FALLO EN LA CREACIÓN DE HILOS\n");
            exit(EXIT_FAILURE);
        }
    }

    // Esperar a que las hebras terminen
    for (i = 0; i < nHebras; i++) {
        pthread_join(threads[i], NULL);
    }

    // Destruye MUTEX y hebras
    pthread_mutex_destroy(&sem_mutex);
    pthread_exit(NULL);

    return 0;
}
