#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

struct ejemplo{
    int inicio;
    int fin;

    /// Retorno
    int valorFinal;
};

typedef struct ejemplo ejemplo;

/////////////////////////
sem_t semaforo;

void* FuncionHebra(void* entrada){

    ejemplo *parametroEntrada = (ejemplo *) entrada;

	//Intentar entrar a la región crítica...
	sem_wait(&semaforo);
    //Revisa si el semáforo es verde! Si es así lo decrementa en 1, y entra...
    //TODA ESTA OPERACIÓN ES ATÓMICA
    int suma = 0;
    ///////REGIÓN CRÍTICA!!!!!
	for(int i = parametroEntrada->inicio; i <= parametroEntrada->fin; i++)
        suma += i;


    ////

    ///LIBREAR (incrementar en 1 el semáforo)
	sem_post(&semaforo);

    parametroEntrada->valorFinal = suma;
    return NULL;
}

//////////////////
int main(){
	///Inicialización 
    sem_init(&semaforo, 0, 1);

	pthread_t hebra1, hebra2;

    ejemplo p1;
    p1.inicio = 1; p1.fin = 10;
	pthread_create(&hebra1,NULL,FuncionHebra, &p1);

    ejemplo p2;
    p2.inicio = 11; p2.fin = 20;
	pthread_create(&hebra2,NULL,FuncionHebra, &p2);
	
    pthread_join(hebra1,NULL);
	pthread_join(hebra2,NULL);

	printf("La Suma Final es %d", (p1.valorFinal + p2.valorFinal));

    sem_destroy(&semaforo);

	return 0;
}
