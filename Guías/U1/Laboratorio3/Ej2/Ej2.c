/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* INTEGRANTES: JORGE CARRILLO
                CRISTÓBAL BRICEÑO */

/******************** PROGRAMA.- ********************/

/*
 * file Ej2.c
 * gcc -g -Wall -pthread -lrt -o Ejercicio2 Ej2.o
 * make Ej2.c
 *      Ej2.o
 *      Ejercicio2
*/

//* Ej2.c - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>  // Librería de hilos.
#include <semaphore.h> // Se añade librería que integra semáforos y mutex (exclusión mutua).

#define _OPEN_THREADS
#define nHebras 5 // Se genera valor constante para cantidad de generación de hebras

sem_t sem_mutex;      // Instancia de semaforo/mutex

int impresiones = 0; // Inicializar variable global


/* Cuerpo del Código */
void *imprimirContenido(void *arg) {  // única función y que imprime el hilo y su ciclo correspondiente, respectivamente.
    // Declaración de variables y de espera de término de proceso de hilo, usando mutex
    int idHilos = (int)arg;

    for (int i = 1; i <= nHebras; i++) {
        sem_wait(&sem_mutex);  // Solicitar el semáforo
        printf("Hilo %d impresiones %d\n", idHilos, impresiones + 1); // Salida de flujo
        impresiones++;
        sleep(1);
        sem_post(&sem_mutex);  // liberar el semáforo
    } printf("\n"); // Salto de linea (mejora la visualización de la impresión)

    pthread_exit(NULL);
}

/* FUNCIÓN PRINCIPAL */
int main() {
    // Declaración de semáforo, mutex, hilos y variables locales
    sem_init(&sem_mutex, 0, 1);
    pthread_t threads[nHebras];
    long i, controlHebras;

    // Ciclo de control y generación de hebras.
    for (i = 1; i <= nHebras; i++) {
        controlHebras = pthread_create(&threads[i], NULL, &imprimirContenido, (void*)i);
        
        if(controlHebras != 0) { // Si el proceso de creación de hilos no fue exitoso, lanza mensaje de advertencia
            printf("\tERROR, FALLO EN LA CREACIÓN DE HILOS\n");
            exit(EXIT_FAILURE);
        }
    }

    // Esperar a que las hebras terminen
    for (i = 1; i <= nHebras; i++) {
        pthread_join(threads[i], NULL);
    }

    // Destruye semáforo y hebras
    sem_destroy(&sem_mutex);
    pthread_exit(NULL);

    return 0;
};