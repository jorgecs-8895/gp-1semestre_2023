/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* INTEGRANTES: JORGE CARRILLO
                CRISTÓBAL BRICEÑO */

/******************** PROGRAMA.- ********************/

/*
 * file Ej3.c
 * gcc -g -Wall -pthread -lrt -o Ejercicio3 Ej3.o
 * make Ej3.c
 *      Ej3.o
 *      Ejercicio3
*/

//* Ejercicio3.c - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>  // Librería de hilos.
#include <semaphore.h> // Se añade librería que integra semáforos y mutex (exclusión mutua).

#define _OPEN_THREADS

#define nHebras 5 // número de hebras
#define M 10 // Parametro numérico de sumas

sem_t sem_mutex;      // Instancia de semaforo/mutex


/* Cuerpo del código */
void *sumaHilos(void *arg) { // Función que imprime los valores asociados (suma) a cada hilo
    // Declaración de variables y de espera de término de proceso de hilo, usando mutex
    int parametroValores = (int)arg;
    int suma = 0;

    // Realizar la suma de los valores dados a los hilos (M)
    for (int i = 0; i < nHebras; i++) {
        suma += i;
    }

    sem_wait(&sem_mutex);     // Solicitar el semáforo y esperar para imprimir el resultado
    printf("- La suma de la hebra ID: %d desde el 1 hasta el %d, es: %d.\n", parametroValores, M, suma); // Imprimir el resultado
    sem_post(&sem_mutex);     // Liberar el semáforo

    pthread_exit((void*)suma); // Retornar el valor de la suma
}

/* Función principal */
int main() { 
    // Declaración de semáforo, mutex, hilos y variables locales
    sem_init(&sem_mutex, 0, 1);
    pthread_t threads[nHebras];
    long i, controlHebras, valor, sumaTotal = 0;
    void *retorno;

    // Ciclo de control y generación de hebras.
    for (i = 0; i < nHebras; i++) {
        controlHebras = pthread_create(&threads[i], NULL, &sumaHilos, (void*)M);

         if(controlHebras != 0) { // Si el proceso de creación de hilos no fue exitoso, lanza mensaje de advertencia
            printf("\tERROR, FALLO EN LA CREACIÓN DE HILOS\n");
            exit(EXIT_FAILURE);
        }
    }

    // Esperar a que las hebras terminen y acumular el resultado.
    for (i = 0; i < nHebras; i++) {
        pthread_join(threads[i], &retorno);
        valor = (int)retorno;
        sumaTotal += valor;
    } printf("\n> La suma total de los valores de las hebras es: %ld.\n\n", sumaTotal); // Imprimir valores acumulados

    // Destruye semáforo y hebras
    sem_destroy(&sem_mutex);
    pthread_exit(NULL);

    return 0;
}
