/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* INTEGRANTES: JORGE CARRILLO
				CRISTÓBAL BRICEÑO */

/******************** PROGRAMA - CLIENTE.- ********************/

/*
 * file ClienteHebras.c
 * gcc ClienteHebras.c -pthread -o Cliente Cliente.o
 * make ClienteHebras.c
 *      Cliente.o
 *      Cliente
 */

//* ClienteHebras.c - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Librerías necesarias para el uso de Sockets
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

// Definir constantes simbólicas
#define PORT 8000
#define BUFFERSIZE 1024


/* Cuerpo del código */

//* Función que crea un nuevo socket
/* Se encarga de crear un nuevo socket en el cliente */
void crearSocket(int *sock){
	if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Error Creación de Socket\n");
		exit(0);
	}
}

//* Función que configura los parámetros de conexión
// Se encarga de los parametros de conexion del cliente antes de establecer contacto con el servidor
void configurarCliente(int sock, struct sockaddr_in *conf){
	conf->sin_family = AF_INET;
	conf->sin_port = htons(PORT);
	conf->sin_addr.s_addr = inet_addr("127.0.0.1");

	// Se establece la conexion con el servidor
	if (connect(sock, (struct sockaddr *)conf, sizeof(*conf)) < 0){
		printf("\nConnection Failed \n");
		exit(0);
	}
}

//* Función Main
int main(int argc, char const *argv[]){
	// Verifica si se proporciona el número de clientes como argumento de línea de comandos.
	if (argc < 2)
		return 0;

	// Genera un arreglo “char” de tamaño 100 y lo copia en la posición definida
	char nombreClientes[100];
	strcpy(nombreClientes, argv[1]);

	// 1. Crear Socket
	int sockCliente;
	crearSocket(&sockCliente);

	// 2. Conectarse al Servior
	struct sockaddr_in confServidor;
	configurarCliente(sockCliente, &confServidor);
   
	int primerMensaje = 1; 
	
	///3. Comunicarse /* Se crean dos buffers de caracteres los cuales se utilizan para almacenar los mansajes enviados y recibidos */
	while(1){
		char buffer[BUFFERSIZE]={0}, buffer2[BUFFERSIZE]={0};

		/* Se envia el mensaje del cliente al servidor usando send */
		if (primerMensaje){
			send(sockCliente, nombreClientes, strlen(nombreClientes), 0);
			primerMensaje = 0;
		
			// Se lee la respuesta del servidor usando “read”
			int valread = read(sockCliente, buffer, BUFFERSIZE);
			fputs(buffer, stdout);
		
		} else {
 			printf("Mensaje: ");
			fgets(buffer, BUFFERSIZE, stdin);
			
			// Si el mensaje ingresado es "BYE" el cliente tiene deseos de finalizar la comunicacion
			if (strcmp(buffer, "BYE\n")==0){
				send(sockCliente, buffer, strlen(buffer), 0);
				
				int valread = read(sockCliente, buffer2, BUFFERSIZE);
				
				strcat(buffer2, nombreClientes);
				fputs(buffer, stdout);
				break;

			} else {	// Si el mensaje ingresado no es "BYE", se envian los mensajes al servidor y se lee la respuesta del servidor
				send(sockCliente, buffer, strlen(buffer), 0);
				int valread = read(sockCliente, buffer, BUFFERSIZE);
				printf("%s",buffer);
			
			}
		}
	}

	close(sockCliente);

	return 0;
}