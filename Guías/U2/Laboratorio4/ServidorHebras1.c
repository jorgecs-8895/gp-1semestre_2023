/*
 * Copyright (C) 2023 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* INTEGRANTES: JORGE CARRILLO
				CRISTÓBAL BRICEÑO */

/******************** PROGRAMA - SERVIDOR.- ********************/

/*
 * file ServidorHebras1.c
 * gcc ServidorHebras1.c -pthread -o Servidor Servidor.o
 * make ServidorHebras1.c
 *      Servidor.o
 *      Servidor
 */

//* ServidorHebras1.c - Programa.

// Llamar e incluir librerías necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Librerías necesarias para el uso de Sockets
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

// Librería necesaria para hilos
#include <pthread.h>

// Definir constantes simbólicas
#define port 8000
#define BUFFERSIZE 1024

/* Cuerpo del código */

//* Función que se encarga de crear un nuevo socket
// Toma un puntero como argumento el cual sirve para almacenar el socket creado
void crearSocket(int *sock) {
	if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Error Creación de Socket\n");
		exit(0);
	}
}

//* Función que se encarga de configurar el servidor
// Configura el servidor con la direccion IP (Local) y el puerto donde se escucha
void configurarServidor(int socket, struct sockaddr_in *conf) {
	conf->sin_family = AF_INET;                // Dominio
	conf->sin_addr.s_addr = htonl(INADDR_ANY); // Enlazar con cualquier dirección local
	conf->sin_port = htons(port);              // Puerto donde escucha

	// Verifica que la funcion bind se ejecuta correctamente
	if ((bind(socket, (struct sockaddr *)conf, sizeof(*conf))) < 0) { // bind!
		printf("Error de enlace\n");
		exit(0);
	}
}

//* Función encargada de escuchar las conexiones entrantes
// Es la encargada de escuchar las conexiones entrantes en el socket del servidor
void escucharClientes(int sock, int n) {
	if (listen(sock, n) < 0) {
		printf("Error listening\n");
		exit(0);
	}
}

//* Función que acepta nuevas conexiones
// Acepta la conexión entrante y crea un nuevo socket dedicado a esa conexión específica
void aceptarConexion(int *sockNuevo, int sock, struct sockaddr_in *conf) {
	// Declaración de variable y asignación de espacio mediante sizeof
	int tamannoConf = sizeof(*conf);

	// Acepta la conexion del cliente en el socket del servidor
	if ((*sockNuevo = accept(sock, (struct sockaddr *)conf, &tamannoConf)) < 0) {
		printf("Error accepting\n");
		exit(0);
	}
}

//* Función que reemplaza caracteres en una cadena “char”
// Se utiliza para reemplazar un carácter por otro carácter
void reemplazarCaracter(char *buffer, char caracter1, char caracter2) {
	for (int j = 0; j < strlen(buffer); j++) {
		if (buffer[j] == caracter1) {
			buffer[j] = caracter2;
		}
	}
}

//* Función que invierte palabras
// Se encarga de recibir una palabra e invertirla
void invertirPalabra(char *palabra) {
	int largo = strlen(palabra);
	char palabra2[largo];

	// “for” que recorre el largo de la cadena para invertir los caracteres
	for (int i = 0; i < largo; i++) {
		palabra2[i] = palabra[largo - i - 1];
	}

	palabra2[largo] = '\0';
	strcpy(palabra, palabra2);
}

//* Función para el hilo del servidor
// Es el hilo que maneja las conexiones de los clientes. Toma el socket del cliente como argumento y realiza la comunicación con el cliente
void *Servidor(void *arg) {
	int *sockCliente = (int *)arg;
	int primerMensaje = 1;
	
	while (1) {
		// Dispone de 3 arreglos tamaño BUFFERSIZE para recibir los parámetros y mantener conexión con el cliente
		char nombre[BUFFERSIZE] = {0};
		char buffer[BUFFERSIZE] = {0};
		char buffer2[BUFFERSIZE] = {0};

		int valread;

		valread = read(*sockCliente, buffer, BUFFERSIZE);

		if (primerMensaje) {	// Saluda al cliente 
			strcpy(buffer2, "Hola ");
			strcat(buffer2, buffer);
			strcat(buffer2, "\n");
			strcpy(nombre, buffer);

			printf("%s", buffer2);
			send(*sockCliente, buffer2, strlen(buffer2), 0);
			primerMensaje = 0;
		} else {	// Si el mensaje es bye, se despide
			if (strcmp(buffer, "BYE\n") == 0) {
				strcpy(buffer2, buffer);
				strcat(buffer2, nombre);
				send(*sockCliente, buffer2, strlen(buffer2), 0);

				printf("%s", buffer2);
				break;
			}

			printf("%s", buffer);

			// Separar en palabras el mensaje recibido
			char buffer2[1024] = {0}, *palabra;
			palabra = strtok(buffer, " ");
			int primeraPalabra = 1;

			while (palabra != NULL) {
				// Invertir palabra
				if (palabra[strlen(palabra) - 1] == '\n') {
					palabra[strlen(palabra) - 1] = '\0';
				}

				invertirPalabra(palabra);

				if (primeraPalabra) {
					strcpy(buffer2, palabra);
					primeraPalabra = 0;
				} else {
					strcat(buffer2, " ");
					strcat(buffer2, palabra);
				}
				palabra = strtok(NULL, " ");
			}
			send(*sockCliente, buffer2, strlen(buffer2), 0);
		}
	}
}

//* Función Main
int main(int argc, char *argv[]) {
	// Verifica si se proporciona el número de clientes como argumento de línea de comandos.
	if (argc < 2)
		return 0;

	int nClientes = strtol(argv[1], NULL, 10);

	if (nClientes < 1){
		return 0;
	}

	// 1. Creación y configuración del Socket
	int sockServidor;
	crearSocket(&sockServidor);

	// 2. Vinculación
	struct sockaddr_in confServidor;
	configurarServidor(sockServidor, &confServidor);

	/// 3. Escuchando conexiones entrantes
	escucharClientes(sockServidor, nClientes);

	// 4. Aceptar conexión
	struct sockaddr_in confCliente;
	int sockCliente;

	// Acepta conexiones de múltiples clientes y crea hebras (hilos) para manejar las conexiones entrantes de manera simultánea.
	for (int i = 0; i < nClientes; i++) {
		aceptarConexion(&sockCliente, sockServidor, &confCliente);

		pthread_t hebra;
		pthread_create(&hebra, NULL, Servidor, (void *)&sockCliente[i]);
	}

	return 0;
}